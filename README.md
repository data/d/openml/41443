# OpenML dataset: Traffic_violations

https://www.openml.org/d/41443

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset contains traffic violation information from all electronic traffic violations issued in the County. Any information that can be used to uniquely identify the vehicle, the vehicle owner or the officer issuing the violation will not be published. For this version, some features were removed and all remaining character features were recoded as nominal factor variables. All punctuation characters were removed from factor levels.
                      The variable 'Violation.Type' is used as target by default. The smaller target categories 'SERO' and 'ESERO' were collapsed into one category labeled 'SERO'.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41443) of an [OpenML dataset](https://www.openml.org/d/41443). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41443/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41443/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41443/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

